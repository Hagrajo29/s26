/*
- What directive is used by Node.js in loading the modules it needs?
	Answer: require function

- What Node.js module contains a method for server creation?
	Answer: http module

- What is the method of the http object responsible for creating a server using Node.js?
	Answer: createServer()

- What method of the response object allows us to set status codes and content types?
	Answer: writeHead

- Where will console.log() output its contents when run in Node.js?
	Answer: port

- What property of the request object contains the address's endpoint?
	Answer: response.end
*/


let http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url === "/login") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to login page.");
	}else if (request.url === "/register"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Server now running at localhost:${port}`);